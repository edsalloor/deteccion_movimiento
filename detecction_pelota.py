import cv2
import numpy as np
import serial
import time

CENTER = 50
POSI = 2

def rotate(img):
    (h, w) = img.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), 90, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(img, M, (nW, nH))

def openning(bin_img, iterations):
    copy = bin_img.copy()
    kernel = np.ones((3,3), np.uint8)
    erosion = cv2.erode(copy, kernel, iterations=iterations)
    dilatacion = cv2.dilate(erosion, kernel, iterations=iterations)
    return dilatacion

def pitch_up(ser,bufferHead):
    bufferHead[0] = POSI
    if(bufferHead[1] < 95):
        bufferHead[1] += 1
    else:
        bufferHead[1] = 95
    ser.write(bufferHead)

def pitch_down(ser,bufferHead):
    bufferHead[0] = POSI
    if(bufferHead[1] < 5):
        bufferHead[1] = 5
    else:
        bufferHead[1] -= 1
    ser.write(bufferHead)

def yaw_right(ser,bufferHead):
    bufferHead[0] = POSI
    if(bufferHead[2] < 95):
        bufferHead[2] += 1
    else:
        bufferHead[2] = 95
    ser.write(bufferHead)

def yaw_left(ser,bufferHead):
    bufferHead[0] = POSI
    if(bufferHead[2] < 5):
        bufferHead[2] = 5
    else:
        bufferHead[2] -= 1
    ser.write(bufferHead)

def rest(ser,bufferHead):
    bufferHead[0] = POSI
    bufferHead[1] = CENTER
    bufferHead[2] = CENTER
    bufferHead[3] = CENTER
    ser.write(bufferHead)
    time.sleep(1)

# Programa para buscar un objeto de color rojo en la imagen de una camara de video
# Si existe mas de un objeto rojo, el programa calcula la posicion del objeto más grande

# Abre driver para capturar video
cap = cv2.VideoCapture(0)                  #camara por defecto
#cap = cv2.VideoCapture("prueba1.avi")       #ruta de archivo de video

ret, frame = cap.read()        #Captura una imagen de la camara y la guarda en la matriz frame
frame=rotate(frame)     #Rotar la imagen
height, width, channels = frame.shape   #Obtener informacion sobre la imagen (ancho, alto, canales)
a=int(width/2)
b=int(height/2)

ser = serial.Serial('COM7', 1000000, timeout=1, parity=serial.PARITY_EVEN, rtscts=1)
bufferHead = [POSI,CENTER,CENTER,CENTER]
rest(ser,bufferHead)

step=0
while(True):
    ret, frame = cap.read() # Captura una imagen de la camara y la guarda en la matriz frame
    # Si llega al final del video no habrá frame
    if (not ret):
        break

    frame=rotate(frame)

    if ((step % 1) == 0):
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV) #Se cambia a color HSV que es mas facil de segmentar

        # mask rojo1 (0-20)
        rojo_bajos1 = np.array([2,50,50])
        rojo_altos1 = np.array([7,255,255])
        mask_rojo1 = cv2.inRange(hsv, rojo_bajos1, rojo_altos1)
        # mask rojo2 (170-255)
    #    rojo_bajos2 = np.array([170,50,50])
    #    rojo_altos2 = np.array([255,255,255])
    #    mask_rojo2 = cv2.inRange(hsv, rojo_bajos2, rojo_altos2)
        # juntar los rojos
    #    mask_rojo = mask_rojo1+mask_rojo2

        #Apertura de la imgen binarizada (máscara de rojo) para eliminar ruido
        opened = openning(mask_rojo1, 6)

        #Obtener los contornos detectados en la imagen abierta
        im2,contornos,jerarquia = cv2.findContours(opened, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        if (len(contornos) > 0):
            area = 0
            pelota = contornos[0]

            #Encontrar el contorno de mayor area
            for cnt in contornos:
                t_area = cv2.contourArea(cnt)
                if (t_area > area):
                    area = t_area
                    pelota = cnt

            #Obtener el circulo circundante minimo del contorno
            (x, y), radius = cv2.minEnclosingCircle(pelota)
            x = int(x)
            y = int(y)
            center = (x, y)
            radius = int(radius)

            #Dibuja un cirulo alrededor de la pelota
            cv2.circle(frame, center, radius, (0, 0, 255), 2)
            cv2.rectangle(frame, center, (x + 2, y + 2), (0, 0, 0), 2)
            #cv2.drawContours(frame, [pelota], 0, (0, 0, 0), 2)

            print("Centro del objeto: ("+str(x)+","+str(y)+")")     # <--- Este es el centro del objeto detectado
            print("Centro de la imagen: (" + str(a) + "," + str(b) + ")")  # <--- Este es el centro de la imagen

            if(y > b):
                pitch_down(ser,bufferHead)
            elif(y < b):
                pitch_up(ser,bufferHead)
            if(x > a):
                yaw_left(ser,bufferHead)
            elif(x < a):
                yaw_right(ser,bufferHead)


    #Dibuja una X en el centro de la imagen
    cv2.line(frame, (width//2-5, height//2-5), (width//2+5, height//2+5),(0,125,255), 2)
    cv2.line(frame, (width//2-5, height//2+5), (width//2+5, height//2-5),(0,125,255), 2)
    cv2.rectangle(frame, (width//2-2, height//2-2), (width//2+2, height//2+2),(255,0,255), 2) #width//2,height//2 es el centro de la imagen

    # Se muestra el resultado
    cv2.imshow("frame",frame)
    cv2.imshow("apertura", opened)
    print(bufferHead)

    ++step
    if cv2.waitKey(1) & 0xFF == ord('q'): # Se chequea si se presiona la tecla q (en la imagen)
        break			      # Si se presiona la tecla q se sale del while

cap.release() # Cierra el driver de la camara
cv2.destroyAllWindows()
serial.close()